package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

var latencyPtr = flag.Int("l", 0, "latency")
var portPtr = flag.String("p", "8081", "port")

func main() {
	flag.Parse()

	http.HandleFunc("/", handler)
	fmt.Println("http-server started\nlisten " + *portPtr + " port\n")
	if *latencyPtr != 0 {
		fmt.Println("with ", *latencyPtr, " sec latency")
	}
	err := http.ListenAndServe(":"+*portPtr, nil)
	if err != nil {
		fmt.Println("An error occurred:", err)
		return
	}
}

func handler(w http.ResponseWriter, r *http.Request) {

	fmt.Println("\n-------------------\n", r.Method, r.URL, r.Proto)
	fmt.Println("Host = ", r.Host)
	fmt.Println("RemoteAddr = " + r.RemoteAddr)
	fmt.Println("Headers = ")
	for k, v := range r.Header {
		fmt.Print(k)
		fmt.Print(" : ")
		fmt.Println(v)
	}
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Println("An error occurred:", err)
		return
	}
	fmt.Println("Body = " + string(b) + "\n-------------------")

	setLatency(*latencyPtr)
	w.WriteHeader(http.StatusOK)
}

func setLatency(s int) {
	time.Sleep(time.Duration(s) * time.Second)
}
