#### Simple http server

serve / 

Just Log POST, GET, DELETE, PUT Requests and get 200 OK Response

launch on 8081 port and with 0 seconds response latency by default

**use -p flag for custom port, like -p=9091**

**use -l flag for response latency in seconds, like -l=20**